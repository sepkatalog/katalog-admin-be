//require('log-timestamp');
require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });

module.exports.error = function error(error, correlation) {
	//console.log('|' + process.env.CONTAINER  + '|ERROR|' + error);
	console.log(`|${correlation}|ERROR|${error}`);
};

module.exports.info = function info(message, correlation) {
	//console.log('|' + process.env.CONTAINER  + '|INFO|' + message);
	console.log(`|${correlation}|INFO |${message}`);
};

module.exports.debug = function debug(message, correlation) {
	//console.log('|' + process.env.CONTAINER  + '|DEBUG|' + message);
	console.log(`|${correlation}|DEBUG|${message}`);
};