module.exports.subjcts = {
    'contactAdmin': 'Message Received [Item Code: {{itemcode}} | Contact: {{contact}}]',
    'contactCx': 'Thanks for your message [Ref: {{itemcode}}]',
    'orderAdmin': 'Order Received [Item ID: {{id}} | QTY: {{qty}}]',
    'orderCx': 'Thanks for the Order [Ref: {{Ref}}]'
}

module.exports.templates = {
    'contactAdmin': '<H1>Message Received [Item Code: {{itemcode}}]</H1><div><p>Item Code: {{itemcode}}</p><p>First Name: {{firstname}}</p><p>Last Name: {{lastname}}</p><p>Address: {{address}}</p><p>Email: {{email}}</p><p>Contact: {{contact}}</p><p>Message: {{message}}</p></div>',
    'contactCx': '<H1>Thanks for your message [Item Code: {{itemcode}}]</H1><div><p>Item Code: {{itemcode}}</p><p>First Name: {{firstname}}</p><p>Last Name: {{lastname}}</p><p>Address: {{address}}</p><p>Email: {{email}}</p><p>Contact: {{contact}}</p><p>Message: {{message}}</p></div>',
    'orderAdmin': '<H1>Order Received [Item ID: {{id}} | QTY: {{qty}}]</H1><div><p>Item ID: {{id}}</p><p>QTY: {{qty}}</p><p>Ref: {{Ref}}</p><p>Date: {{createdAt}}</p><p>First Name: {{firstname}}</p><p>Last Name: {{lastname}}</p><p>Address: {{address}}</p><p>Email: {{email}}</p><p>Contact: {{contact}}</p></div>',
    'orderCx': '<H1>Order Received [Item ID: {{id}} | QTY: {{qty}}]</H1><div><p>Item ID: {{id}}</p><p>QTY: {{qty}}</p><p>Ref: {{Ref}}</p><p>Date: {{createdAt}}</p><p>First Name: {{firstname}}</p><p>Last Name: {{lastname}}</p><p>Address: {{address}}</p><p>Email: {{email}}</p><p>Contact: {{contact}}</p></div>'
}