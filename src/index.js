var cron = require('node-cron');
const app = require('./server');
const sequelize = require('./sequelize');

const PORT = process.env.PORT || 4000;

async function assertDatabaseConnectionOk() {
	console.log('Checking database connection...');
	try {
		await sequelize.authenticate();
		console.log('Database connection OK!');
	} catch (error) {
		console.log('Unable to connect to the database:');
		console.log(error.message);
		process.exit(1);
	}
}

async function init() {
	await assertDatabaseConnectionOk();

	console.log(`Starting Sequelize + Express on port ${PORT}...`);

	app.listen(PORT, () => {
		console.log(`Express server started on port ${PORT}. Try some routes, such as '/api/users'.`);
	});

	initCrons();
}

async function initCrons() {
	cron.schedule('* * * * *',()=>{
		console.log('Running  schedulers every minute');
		//You can all schedulers here. as of now now any schedulers.
	});
}

init();


 