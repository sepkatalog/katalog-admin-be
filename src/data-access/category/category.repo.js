var logger = require('../../helper/logger');
const logVar = ' Repo | item |';
const { Sequelize } = require('sequelize');
const { uploadFile } = require('../../helper/s3');
const fs = require('fs');

module.exports = function buildCustomerRepository(dataSource) {
    return Object.freeze({
        getCategories
    });

    async function getCategories(correlationId) {
        logger.debug(logVar + "in get categories", correlationId);

        return new Promise((resolve, reject) => {
            dataSource.Category.findAll().then(data => {
                resolve(data);
            }).catch((error) => {
                logger.debug(logVar + " ERROR | " + error, correlationId);
                reject({
                    statusCode: 500,
                    message: error
                });
            })

        });
    }
};