var logger = require('../../helper/logger');
const logVar = ' Repo | Order |';
const { Sequelize } = require('sequelize');
const { v4 } = require('uuid');

module.exports = function buildOrderRepository(dataSource) {
    return Object.freeze({
        addOrder,
        getOrders
    });

    async function addOrder(req, correlationId) {

        logger.debug(logVar + "in add order", correlationId);

        // productid
        // productprice
        // totalprice
        // qty

        return new Promise((resolve, reject) => {

            dataSource.Item.findOne({
                where: {
                    id: req.productid
                }
            }).then((item) => {

                req.productprice = item.price;
                req.totalprice = item.price * req.qty;
                req.code =  v4();

                dataSource.Order.create(req).then((order) => {
                    order.setItem(item);
                    resolve(order)
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            })
        });


        // return new Promise((resolve, reject) => {
        //     dataSource.Item.create(req).then(data => {
        //         resolve(data);
        //     }).catch((error) => {

        //         logger.debug(logVar + " ERROR | " + error, correlationId);
        //         reject({
        //             statusCode: 500,
        //             message: error
        //         });
        //     })
        // });
    }

    async function getOrders(offset, limit, code, correlationId) {
        logger.debug(logVar + "in get orders" + " | offset:" + offset + " | limit:" + limit, correlationId);

        return new Promise((resolve, reject) => {
            if (code === undefined || code === '') {
                dataSource.Order.findAll({
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ],
                    include: [{
                        model: dataSource.Item, as: 'Item',
                    }]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            } else {
                dataSource.Order.findAll({
                    where: {
                        code: code
                    },
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ],
                    include: [{
                        model: dataSource.Item, as: 'Item',
                    }]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            }
        });
    }
};