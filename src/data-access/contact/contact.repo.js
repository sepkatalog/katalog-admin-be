var logger = require('../../helper/logger');
const logVar = ' Repo | Contact |';
const { Sequelize } = require('sequelize');
const { v4 } = require('uuid');

module.exports = function buildContactRepository(dataSource) {
    return Object.freeze({
        addContact,
        getContacts
    });

    async function addContact(req, correlationId) {

        logger.debug(logVar + "in add contact", correlationId);

        return new Promise((resolve, reject) => {

            dataSource.Contact.create(req).then((contact) => {
                resolve(contact)
            }).catch((error) => {
                logger.debug(logVar + " ERROR | " + error, correlationId);
                reject({
                    statusCode: 500,
                    message: error
                });
            })
        });


        // return new Promise((resolve, reject) => {
        //     dataSource.Item.create(req).then(data => {
        //         resolve(data);
        //     }).catch((error) => {

        //         logger.debug(logVar + " ERROR | " + error, correlationId);
        //         reject({
        //             statusCode: 500,
        //             message: error
        //         });
        //     })
        // });
    }

    async function getContacts(offset, limit, id, correlationId) {
        logger.debug(logVar + "in get contacts" + " | offset:" + offset + " | limit:" + limit, correlationId);

        return new Promise((resolve, reject) => {
            if (id === undefined || id === '') {
                dataSource.Contact.findAll({
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            } else {
                dataSource.Contact.findAll({
                    where: {
                        id: id
                    },
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            }
        });
    }
};