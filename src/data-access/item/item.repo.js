var logger = require('../../helper/logger');
const logVar = ' Repo | item |';
const { Sequelize } = require('sequelize');
const { uploadFile } = require('../../helper/s3');
const fs = require('fs');

module.exports = function buildCustomerRepository(dataSource) {
    return Object.freeze({
        addItem,
        getItems,
        getItemById,
        updateItem,
        deleteItem
    });

    async function addItem(req, correlationId) {

        logger.debug(logVar + "in add item", correlationId);

        const thumbNail = req.title + "thumbNail.jpg";
        const bigNail = req.title + "bigNail.jpg";

        uploadFile(req.thumbnail, thumbNail);
        uploadFile(req.image, bigNail);

        const s3Bucket = ""

        req.thumbnail = "https://sep-katalog-two.s3.ap-southeast-1.amazonaws.com/" + thumbNail;
        req.image = "https://sep-katalog-two.s3.ap-southeast-1.amazonaws.com/" + bigNail;

        req.keywards = req.code + " " + req.title + " " + req.description + " " + req.specification + " " + req.price + " " + req.status;

        return new Promise((resolve, reject) => {
            dataSource.Item.create(req).then(data => {
                resolve(data);
            }).catch((error) => {

                logger.debug(logVar + " ERROR | " + error, correlationId);
                reject({
                    statusCode: 500,
                    message: error
                });
            })
        });
    }

    async function getItems(offset, limit, keywards, category, correlationId) {
        logger.debug(logVar + "in get items" + " | offset:" + offset + " | limit" + limit, correlationId);

        return new Promise((resolve, reject) => {
            if (category === undefined) {
                dataSource.Item.findAll({
                    where: {
                        keywards: {
                            [Sequelize.Op.like]: '%' + keywards + '%'
                        }
                    },
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ],
                    include: [{
                        model: dataSource.Category, as: 'Category',
                    }]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            } else {


                dataSource.Item.findAll({
                    where: {
                        keywards: {
                            [Sequelize.Op.like]: '%' + keywards + '%'
                        },
                        category: category
                    },
                    offset: parseInt(offset),
                    limit: parseInt(limit),
                    order: [
                        ['id', 'DESC'],
                    ],
                    include: [{
                        model: dataSource.Category, as: 'Category',
                    }]
                }).then(data => {
                    resolve(data);
                }).catch((error) => {
                    logger.debug(logVar + " ERROR | " + error, correlationId);
                    reject({
                        statusCode: 500,
                        message: error
                    });
                })
            }
        });
    }

    async function getItemById(itemId, correlationId) {
        logger.debug(logVar + "in get item by id");

        return new Promise((resolve, reject) => {
            dataSource.Item.findOne({
                where: { id: parseInt(itemId) },
                include: [{
                    model: dataSource.Category, as: 'Category',
                }]
            }).then(data => {
                resolve(data);
            }).catch((error) => {
                logger.debug(logVar + " ERROR | " + error, correlationId);
                reject({
                    statusCode: 500,
                    message: error
                });
            })
        });
    }

    async function updateItem(req) {
        logger.debug(logVar + "in update item");

        const thumbNail = req.title + "thumbNail.jpg";
        const bigNail = req.title + "bigNail.jpg";

        uploadFile(req.thumbnail, thumbNail);
        uploadFile(req.image, bigNail);

        const s3Bucket = ""

        req.thumbnail = "https://sep-katalog-two.s3.ap-southeast-1.amazonaws.com/" + thumbNail;
        req.image = "https://sep-katalog-two.s3.ap-southeast-1.amazonaws.com/" + bigNail;

        req.keywards = req.code + " " + req.title + " " + req.description + " " + req.specification + " " + req.price + " " + req.status;

        return new Promise((resolve, reject) => {
            dataSource.Item.update(req, {
                where: { id: req.id }
            }).then(data => {

                resolve(req);

            }).catch((error) => {
                console.log(error);
                reject({
                    statusCode: 500,
                    message: error
                });
            })
        });
    }

    async function deleteItem(req) {
        logger.debug(logVar + "in delete item");

        return new Promise((resolve, reject) => {
            const deleteSetup = {
                "status": "DELETED"
            }

            dataSource.Item.destroy({
                where: { id: req }
            }).then(data => {
                resolve(req);
            }).catch((error) => {
                console.log(error);
                reject({
                    statusCode: 500,
                    message: error
                });
            })
        });
    }
};