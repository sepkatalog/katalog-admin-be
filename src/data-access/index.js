const buildItemRepository = require('./item/item.repo');
const buildCategoryRepository = require('./category/category.repo');
const buildOrderRepository = require('./order/order.repo');
const buildContactRepository = require('./contact/contact.repo');

const { models } = require('../sequelize');

module.exports.itemRepository = buildItemRepository(models);
module.exports.categoryRepository = buildCategoryRepository(models);
module.exports.orderRepository = buildOrderRepository(models);
module.exports.contactRepository = buildContactRepository(models);