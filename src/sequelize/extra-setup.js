
function applyExtraSetup(sequelize) {
	const {
		Item,
		Category,
		Order,
		Contact
	} = sequelize.models;

	Category.hasMany(Item, { foreignKey: 'categoryId', as: 'Item', onDelete: 'RESTRICT' });
	Item.belongsTo(Category, { foreignKey: 'categoryId', as: 'Category', onDelete: 'RESTRICT' });

	Item.hasMany(Order, { foreignKey: 'itemId', as: 'Order', onDelete: 'RESTRICT' });
	Order.belongsTo(Item, { foreignKey: 'itemId', as: 'Item', onDelete: 'RESTRICT' });

	// Header.hasMany(Customer, { foreignKey: 'headerId', as: 'Customer', onDelete: 'RESTRICT' });
	// Customer.belongsTo(Header, { foreignKey: 'headerId', as: 'Header', onDelete: 'RESTRICT' });

	// Header.hasMany(Product, { foreignKey: 'headerId', as: 'Product', onDelete: 'RESTRICT' });
	// Product.belongsTo(Header, { foreignKey: 'headerId', as: 'Header', onDelete: 'RESTRICT' });

	// Header.hasMany(User, { foreignKey: 'headerId', as: 'User', onDelete: 'RESTRICT' });
	// User.belongsTo(Header, { foreignKey: 'headerId', as: 'Header', onDelete: 'RESTRICT' });
}

module.exports = { applyExtraSetup };	
