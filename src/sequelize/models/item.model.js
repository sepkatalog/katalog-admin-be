const { DataTypes, Sequelize } = require('sequelize');

// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('Item', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		code: {
			allowNull: false,
			type: DataTypes.STRING(10),
			unique: true
		},
		title: {
			allowNull: false,
			type: DataTypes.STRING(100),
			unique: false
		},
		description: {
			allowNull: true,
			type: DataTypes.STRING(300),
			unique: false
		},
        specification: {
			allowNull: true,
			type: DataTypes.STRING(200),
			unique: false
		},
        thumbnail: {
			allowNull: true,
			type: DataTypes.STRING(1024),
			unique: false
		},
        image: {
			allowNull: true,
			type: DataTypes.STRING(1024),
			unique: false
		},
		price: {
			allowNull: false,
			type: DataTypes.DECIMAL(10,2),
            unique: false
		},
		category: {
			allowNull: false,
			type: DataTypes.INTEGER,
			unique: false
		},
		status: {
			allowNull: false,
			type: DataTypes.STRING(20),
			unique: false,
			defaultValue: 'IN STOCK'
		},
		keywards: {
			allowNull: true,
			type: DataTypes.STRING(1000),
			unique: false
		},
		createdBy: {
			allowNull: false,
			type: DataTypes.STRING(20),
            unique: false
		},
		updatedBy: {
			allowNull: true,
			type: DataTypes.STRING(20),
            unique: false
		}
	}, {
		tableName: 'items'
	});
};