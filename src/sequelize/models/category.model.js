const { DataTypes, Sequelize } = require('sequelize');

// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('Category', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		code: {
			allowNull: false,
			type: DataTypes.STRING(10),
			unique: true
		},
		title: {
			allowNull: false,
			type: DataTypes.STRING(100),
			unique: false
		},
		status: {
			allowNull: false,
			type: DataTypes.STRING(20),
			unique: false,
			defaultValue: 'INITIATED'
		},
		createdBy: {
			allowNull: false,
			type: DataTypes.STRING(20),
            unique: false
		},
		updatedBy: {
			allowNull: true,
			type: DataTypes.STRING(20),
            unique: false
		}
	}, {
		tableName: 'categories'
	});
};