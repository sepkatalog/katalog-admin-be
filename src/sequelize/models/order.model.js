const { DataTypes, Sequelize } = require('sequelize');

// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('Order', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		code: {
			allowNull: false,
			type: DataTypes.UUID,
			defaultValue: Sequelize.UUID4,
			unique: true
		},
		qty: {
			allowNull: false,
			type: DataTypes.INTEGER,
			unique: false
		},
		firstname: {
			allowNull: false,
			type: DataTypes.STRING(300),
			unique: false
		},
		lastname: {
			allowNull: true,
			type: DataTypes.STRING(300),
			unique: false
		},
		address: {
			allowNull: false,
			type: DataTypes.STRING(500),
			unique: false
		},
		email: {
			allowNull: true,
			type: DataTypes.STRING(300),
			unique: false
		},
		contact: {
			allowNull: false,
			type: DataTypes.STRING(300),
			unique: false
		},
		productid: {
			allowNull: false,
			type: DataTypes.INTEGER,
			unique: false
		},
		productprice: {
			allowNull: false,
			type: DataTypes.DECIMAL(10,2),
            unique: false
		},
		totalprice: {
			allowNull: false,
			type: DataTypes.DECIMAL(10,2),
            unique: false
		},
		createdBy: {
			allowNull: false,
			type: DataTypes.STRING(20),
            unique: false
		},
		updatedBy: {
			allowNull: true,
			type: DataTypes.STRING(20),
            unique: false
		}
	}, {
		tableName: 'orders'
	});
};