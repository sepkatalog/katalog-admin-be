const { Sequelize } = require('sequelize');
const { applyExtraSetup } = require('./extra-setup');
const dbConfig = require('../config').MY_SQL;

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USERNAME, dbConfig.PASSWORD, {
	host: dbConfig.HOST,
	dialect: dbConfig.dialect,

	pool: {
		max: dbConfig.pool.max,
		min: dbConfig.pool.min,
		acquire: dbConfig.pool.acquire,
		idle: dbConfig.pool.idle
	},

	define: {
		freezeTableName: false
	},
	dialectOptions: {
		//	useUTC: false, //for reading from database
		dateStrings: true,
		typeCast: true
	},
	timezone: '+05:30',//for writing to database
	logging: false
});

const modelDefiners = [
	require('./models/item.model'),
	require('./models/category.model'),
	require('./models/order.model'), 
	require('./models/contactus.model')
];

// We define all models according to their files.
for (const modelDefiner of modelDefiners) {
	modelDefiner(sequelize);
}

// We execute any extra setup after the models are defined, such as adding associations.
applyExtraSetup(sequelize);

// In Prod
sequelize.sync();
// in Prod - load banks and branches

// sequelize.sync().then(() => {
// 	bankAndBranchSeed.insert(sequelize.models);
// });

//In Dev
// sequelize.sync({ force: true }).then(() => {
// 	console.log('Drop and re-sync db.');
// });

//In Dev
// sequelize.sync({
// 	force: true
// }).then(() => {
// 	seed.insert(sequelize.models);
// }).then(() => {
// 	console.log('Drop and re-sync db.');
// });

// We export the sequelize connection instance to be used around our app.
module.exports = sequelize;
