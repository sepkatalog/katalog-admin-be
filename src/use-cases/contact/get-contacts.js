var logger = require('../../helper/logger');
const logVar = ' Usecase | get-contacts |';

module.exports = function buildGetContacts(contactRepo) {
	const getContacts = async (offset, limit, id, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await contactRepo.getContacts(offset, limit, id, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return getContacts;
};