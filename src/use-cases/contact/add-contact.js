var logger = require('../../helper/logger');
const logVar = ' Usecase | add-contact |';
var emaller = require('../../helper/sendEmail');
var emaiTemplates = require('../../helper/emailTemplates');

module.exports = function buildAddContact(contactRepo) {
	const addContact = async (contact, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

			const iData = await contactRepo.addContact(contact, correlationId);
			const subjectAdmin = emaiTemplates.subjcts.contactAdmin.replace('{{itemcode}}', contact.itemcode).replace('{{contact}}', contact.contact);
			const subjectCx = emaiTemplates.subjcts.contactCx.replace('{{itemcode}}', contact.itemcode);

			var body = emaiTemplates.templates.contactAdmin;

			body = body.replace('{{itemcode}}', contact.itemcode);
			body = body.replace('{{itemcode}}', contact.itemcode);
			body = body.replace('{{firstname}}', contact.firstname);
			body = body.replace('{{lastname}}', contact.lastname);
			body = body.replace('{{address}}', contact.address);
			body = body.replace('{{email}}', contact.email);
			body = body.replace('{{contact}}', contact.contact);
			body = body.replace('{{message}}', contact.message);

			let messageAdmin = {
				from: "katalogmsc@gmail.com",
				to: "katalogmsc@gmail.com",
				subject: subjectAdmin,
				html: body
			}
			emaller.sendMail(messageAdmin);

			let messageCx = {
				from: "katalogmsc@gmail.com",
				to: contact.email,
				subject: subjectCx,
				html: body
			}
			emaller.sendMail(messageCx);


			return iData;

		} catch (ex) {
			logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return addContact;
};