const repositories = require('../data-access');

const buildAddItem = require('./item/add-item');
const buildGetItems = require('./item/get-items');
const buildGetItemById = require('./item/get-item-by-id');
const buildUpdateItem = require('./item/update-item');
const buildDeleteItem = require('./item/delete-item');

const buildGetCategories = require('./cateegory/get-categories');

const buildAddOrder = require('./order/add-order');
const buildGetOrders = require('./order/get-orders');

const buildAddContact= require('./contact/add-contact');
const buildGetContacts= require('./contact/get-contacts');

module.exports.addItem = buildAddItem(repositories.itemRepository);
module.exports.getItems = buildGetItems(repositories.itemRepository);
module.exports.getItemById = buildGetItemById(repositories.itemRepository);
module.exports.updateItem = buildUpdateItem(repositories.itemRepository);
module.exports.deleteItem = buildDeleteItem(repositories.itemRepository);

module.exports.getCategories = buildGetCategories(repositories.categoryRepository);

module.exports.addOrder = buildAddOrder(repositories.orderRepository);
module.exports.getOrders = buildGetOrders(repositories.orderRepository);

module.exports.addContact = buildAddContact(repositories.contactRepository);
module.exports.getContacts = buildGetContacts(repositories.contactRepository);