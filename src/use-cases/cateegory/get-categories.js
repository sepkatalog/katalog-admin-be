var logger = require('../../helper/logger');
const logVar = ' Usecase | get-categories |';

module.exports = function buildGetCategories(categoryRepo) {
	const getCategories = async (correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await categoryRepo.getCategories(correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return getCategories;
};