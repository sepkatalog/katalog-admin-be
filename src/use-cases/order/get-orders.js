var logger = require('../../helper/logger');
const logVar = ' Usecase | get-orders |';

module.exports = function buildGetOrders(orderRepo) {
	const getOrders = async (offset, limit, code, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await orderRepo.getOrders(offset, limit, code, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return getOrders;
};