var logger = require('../../helper/logger');
const logVar = ' Usecase | add-order |';
var emaller = require('../../helper/sendEmail');
var emaiTemplates = require('../../helper/emailTemplates');


module.exports = function buildAddOrder(orderRepo) {
	const addOrder = async (order, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await orderRepo.addOrder(order, correlationId);
            
			const subjectAdmin = emaiTemplates.subjcts.orderAdmin.replace('{{id}}',order.productid).replace('{{qty}}',order.qty);
			const subjectCx = emaiTemplates.subjcts.orderCx.replace('{{id}}',order.productid).replace('{{qty}}',order.qty).replace('{{Ref}}',iData.code);
			
			var body = emaiTemplates.templates.orderAdmin;
			body = body.replace('{{Ref}}',iData.code);
			body = body.replace('{{createdAt}}',iData.createdAt);
			body = body.replace('{{id}}',order.productid);
			body = body.replace('{{id}}',order.productid);
			body = body.replace('{{qty}}',order.qty);
			body = body.replace('{{qty}}',order.qty);
			body = body.replace('{{firstname}}',order.firstname);
			body = body.replace('{{lastname}}',order.lastname);
			body = body.replace('{{address}}',order.address);
			body = body.replace('{{email}}',order.email);
			body = body.replace('{{contact}}',order.contact);

			let messageAdmin = {
				from: "katalogmsc@gmail.com",
				to: "katalogmsc@gmail.com",
				subject: subjectAdmin,
				html: body
			}
			emaller.sendMail(messageAdmin);
			let messageCx = {
				from: "katalogmsc@gmail.com",
				to: order.email,
				subject: subjectCx,
				html: body
			}
			emaller.sendMail(messageCx);


            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return addOrder;
};