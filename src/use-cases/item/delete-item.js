var logger = require('../../helper/logger');
const logVar = ' Usecase | delete-item |';

module.exports = function buildDeleteItem(itemRepo) {
	const deleteItem = async (item, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await itemRepo.deleteItem(item, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return deleteItem;
};