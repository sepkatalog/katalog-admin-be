var logger = require('../../helper/logger');
const logVar = ' Usecase | add-item |';

module.exports = function buildAddItem(itemRepo) {
	const addItem = async (item, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await itemRepo.addItem(item, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return addItem;
};