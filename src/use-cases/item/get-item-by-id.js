var logger = require('../../helper/logger');
const logVar = ' Usecase | get-item-by-id |';

module.exports = function buildGetItemById(itemRepo) {
	const getItemById = async (itemId, correlationId) => {

		logger.debug(logVar + " Item ID | " + itemId, correlationId);

		try {

            const iData = await itemRepo.getItemById(itemId, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return getItemById;
};