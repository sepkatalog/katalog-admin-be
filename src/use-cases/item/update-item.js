var logger = require('../../helper/logger');
const logVar = ' Usecase | update-item |';

module.exports = function buildUpdateItem(itemRepo) {
	const updateItem = async (item, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await itemRepo.updateItem(item, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return updateItem;
};