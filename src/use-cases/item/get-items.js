var logger = require('../../helper/logger');
const logVar = ' Usecase | get-items |';

module.exports = function buildGetItems(itemRepo) {
	const getItems = async (offset, limit, keywards, category, correlationId) => {

		logger.debug(logVar, correlationId);

		try {

            const iData = await itemRepo.getItems(offset, limit, keywards, category, correlationId);
            
            return iData;

		} catch (ex) {
            logger.debug(logVar + "ERROR | " + ex, correlationId);
			return ex;
		}
	};

	return getItems;
};