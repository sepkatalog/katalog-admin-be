const express = require('express');
var routes = require('./routes');
var cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

const keycloak = require('./keycloak-config').initKeycloak();
app.use(keycloak.middleware());

app.use(bodyParser.json({limit:'6mb'}));
app.use(express.json({ type: 'application/json' }));
app.use(express.urlencoded({ extended: false }));
app.use(cors());


app.use('/api/v1/', routes);

app.get('/', (req, res) => {
	// debug(req);
	res.send('Sample GET');
});

app.post('/', (req, res) => {
	// debug(req);
	console.log(req);
	res.send('Sample POST');
});

app.put('/', (req, res) => {
	// debug(req);
	console.log(req);
	res.send('Sample PUT');
});
app.delete('/', (req, res) => {
	// debug(req);
	console.log(req);
	res.send('Sample delete');
});


module.exports = app;