const express = require('express');
const { postItem, getItems, getItemById, updateItem, deleteItem} = require('../controllers');
const callBack = require('../express-callback');
const router = express.Router();
const keycloak = require('../keycloak-config').initKeycloak();

router.post('/', callBack(postItem));
router.get('/:id', callBack(getItemById));
router.get('/', callBack(getItems));
router.put('/', callBack(updateItem));
router.delete('/:id', callBack(deleteItem));

// router.post('/', keycloak.protect('realm:katalog-admin'), callBack(postItem));
// router.get('/:id', keycloak.protect('realm:katalog-admin'), callBack(getItemById));
// router.get('/', keycloak.protect('realm:katalog-admin'), callBack(getItems));
// router.put('/', keycloak.protect('realm:katalog-admin'), callBack(updateItem));
// router.delete('/', keycloak.protect('realm:katalog-admin'), callBack(deleteItem));

// router.post('/', callBack(postItem));
// router.get('/:id', callBack(getItemById));
// router.get('/', callBack(getItems));


module.exports = router;