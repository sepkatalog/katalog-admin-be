const express = require('express');
const { getCategories } = require('../controllers');
const callBack = require('../express-callback');
const router = express.Router();
const keycloak = require('../keycloak-config').initKeycloak();

router.get('/', callBack(getCategories));

// router.post('/', keycloak.protect('realm:katalog-admin'), callBack(postItem));
// router.get('/:id', keycloak.protect('realm:katalog-admin'), callBack(getItemById));
// router.get('/', keycloak.protect('realm:katalog-admin'), callBack(getItems));
// router.put('/', keycloak.protect('realm:katalog-admin'), callBack(updateItem));
// router.delete('/', keycloak.protect('realm:katalog-admin'), callBack(deleteItem));

// router.post('/', callBack(postItem));
// router.get('/:id', callBack(getItemById));
// router.get('/', callBack(getItems));


module.exports = router;