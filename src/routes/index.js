const express = require('express');
const itemRouter = require('./item.router');
const categoryRouter = require('./category.router');
const orderRouter = require('./order.router');
const contactRouter = require('./contact.router');

const router = express.Router();
router.use('/item', itemRouter);
router.use('/category', categoryRouter);
router.use('/order', orderRouter);
router.use('/contact', contactRouter);

module.exports = router;
