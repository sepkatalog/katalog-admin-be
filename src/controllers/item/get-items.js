//const JwtTokenDecoder = require("../../helper/auth-jwt-decoder");
var logger = require('../../helper/logger');
const logVar = ' Controller | get-items |';
var uuid = require('uuid');

module.exports = function buildGetItems(getItemsUseCase) {
	return async function getItems(httpRequest) {

		const correlationId = uuid.v4();

		logger.debug(logVar + " In get items", correlationId);

		var page = httpRequest.query.page;
		var limit = httpRequest.query.limit;
		var keywards = httpRequest.query.kws;
		var category = httpRequest.query.cat;

		if (page == undefined || page == null || page == NaN) {
			page = 0;
		} else {
			page = parseInt(page);
		}

		if (limit == undefined || limit == null || limit == NaN) {
			limit = 10;
		} else {
			limit = parseInt(limit);
		}

		if (keywards == undefined || keywards == null || keywards == NaN) {
			keywards = "";
		} 

		const offset = page * limit;

		const headers = {
			'Content-Type': 'application/json'
		};

		try {

			logger.debug(logVar + ' header |' + JSON.stringify(httpRequest.headers), correlationId);
			logger.debug(logVar + ' body |' + JSON.stringify(httpRequest.body), correlationId);

			logger.debug(logVar + ' limit: ' + limit, correlationId);
			logger.debug(logVar + ' offset: ' + offset, correlationId);
			logger.debug(logVar + ' keywards: ' + keywards, correlationId);
			logger.debug(logVar + ' category: ' + category, correlationId);

			let items = await getItemsUseCase(offset, limit, keywards, category, correlationId);

			return {
				headers,
				statusCode: 200,
				body: items
			};

			// let productData = await postProductUseCase(httpRequest.body);

			// if (productData.statusCode === undefined) {
			// 	productData = transfomer.productResponse(productData);
			// 	return {
			// 		headers,
			// 		statusCode: 200,
			// 		body: productData
			// 	};
			// } else {
			// 	logger.error(logVar + JSON.stringify(productData));
			// 	return {
			// 		headers,
			// 		statusCode: productData.statusCode,
			// 		body: {
			// 			errorCode: productData.statusCode,
			// 			errorDescription: productData.message.errors[0].message
			// 		}
			// 	};
			// }

		} catch (e) {
			logger.debug(logVar + ' ERROR |' + JSON.stringify(e), correlationId);
			return {
				headers,
				statusCode: 500,
				body: {
					errorCode: 500,
					errorDescription: e.message
				}
			};
		}
	};
};