//const JwtTokenDecoder = require("../../helper/auth-jwt-decoder");
var logger = require('../../helper/logger');
const logVar = ' Controller | get-item-by-id |';
var uuid = require('uuid');

module.exports = function buildGetItemById(getItemByIdUseCase) {
	return async function getItemById(httpRequest) {

		const correlationId = uuid.v4();

		logger.debug(logVar + " In get item By Id", correlationId);

		const headers = {
			'Content-Type': 'application/json'
		};

		try {

			logger.debug(logVar + ' header |' + JSON.stringify(httpRequest.headers), correlationId);
			logger.debug(logVar + ' body |' + JSON.stringify(httpRequest.body), correlationId);

			const itemId = httpRequest.params.id;

			let items = await getItemByIdUseCase(itemId, correlationId);

			return {
				headers,
				statusCode: 200,
				body: items
			};

			// let productData = await postProductUseCase(httpRequest.body);

			// if (productData.statusCode === undefined) {
			// 	productData = transfomer.productResponse(productData);
			// 	return {
			// 		headers,
			// 		statusCode: 200,
			// 		body: productData
			// 	};
			// } else {
			// 	logger.error(logVar + JSON.stringify(productData));
			// 	return {
			// 		headers,
			// 		statusCode: productData.statusCode,
			// 		body: {
			// 			errorCode: productData.statusCode,
			// 			errorDescription: productData.message.errors[0].message
			// 		}
			// 	};
			// }

		} catch (e) {
			logger.debug(logVar + ' ERROR |' + JSON.stringify(e), correlationId);
			return {
				headers,
				statusCode: 500,
				body: {
					errorCode: 500,
					errorDescription: e.message
				}
			};
		}
	};
};