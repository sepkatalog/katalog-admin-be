//const JwtTokenDecoder = require("../../helper/auth-jwt-decoder");
var logger = require('../../helper/logger');
const logVar = ' Controller | get-contacts |';
var uuid = require('uuid');

module.exports = function buildGetContacts(getContactsUseCase) {
	return async function getContacts(httpRequest) {

		const correlationId = uuid.v4();

		logger.debug(logVar + " In get orders", correlationId);

		var page = httpRequest.query.page;
		var limit = httpRequest.query.limit;
		var id = httpRequest.query.id;

		if (page == undefined || page == null || page == NaN) {
			page = 0;
		} else {
			page = parseInt(page);
		}

		if (limit == undefined || limit == null || limit == NaN) {
			limit = 10;
		} else {
			limit = parseInt(limit);
		}

		if (id == undefined || id == null || id == NaN) {
			id = "";
		} 

		const offset = page * limit;

		const headers = {
			'Content-Type': 'application/json'
		};

		try {

			logger.debug(logVar + ' header |' + JSON.stringify(httpRequest.headers), correlationId);
			logger.debug(logVar + ' body |' + JSON.stringify(httpRequest.body), correlationId);

			logger.debug(logVar + ' limit: ' + limit, correlationId);
			logger.debug(logVar + ' offset: ' + offset, correlationId);
			logger.debug(logVar + ' id: ' + id, correlationId);
			
			let contacts = await getContactsUseCase(offset, limit, id, correlationId);

			return {
				headers,
				statusCode: 200,
				body: contacts
			};

			// let productData = await postProductUseCase(httpRequest.body);

			// if (productData.statusCode === undefined) {
			// 	productData = transfomer.productResponse(productData);
			// 	return {
			// 		headers,
			// 		statusCode: 200,
			// 		body: productData
			// 	};
			// } else {
			// 	logger.error(logVar + JSON.stringify(productData));
			// 	return {
			// 		headers,
			// 		statusCode: productData.statusCode,
			// 		body: {
			// 			errorCode: productData.statusCode,
			// 			errorDescription: productData.message.errors[0].message
			// 		}
			// 	};
			// }

		} catch (e) {
			logger.debug(logVar + ' ERROR |' + JSON.stringify(e), correlationId);
			return {
				headers,
				statusCode: 500,
				body: {
					errorCode: 500,
					errorDescription: e.message
				}
			};
		}
	};
};