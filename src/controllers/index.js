const usecases = require('../use-cases');

const buildPostItem = require('./item/post-item');
const buildGetItems = require('./item/get-items');
const buildGetItemById = require('./item/get-item-by-id');
const buildUpdateItems = require('./item/update-item');
const buildDeleteItemById = require('./item/delete-item');

const buildGetCategories = require('./category/get-categories');

const buildPostOrder = require('./order/post-order');
const buildGetOrders = require('./order/get-orders');
const buildPostContact = require('./contact/post-contact');
const buildGetContacts = require('./contact/get-contacts');


module.exports.postItem = buildPostItem(usecases.addItem);
module.exports.getItems = buildGetItems(usecases.getItems);
module.exports.getItemById = buildGetItemById(usecases.getItemById);
module.exports.updateItem = buildUpdateItems(usecases.updateItem);
module.exports.deleteItem = buildDeleteItemById(usecases.deleteItem);

module.exports.getCategories = buildGetCategories(usecases.getCategories);

module.exports.postOrder = buildPostOrder(usecases.addOrder);
module.exports.getOrders = buildGetOrders(usecases.getOrders);

module.exports.postContact = buildPostContact(usecases.addContact);
module.exports.getContacts = buildGetContacts(usecases.getContacts);

