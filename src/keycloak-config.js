var session = require('express-session');
var Keycloak = require('keycloak-connect');

let _keycloak;

var keycloakConfig = {
	'realm': 'katalog',
	'bearer-only': true,
	'auth-server-url': 'http://52.91.106.182:8080/auth/',
	'ssl-required': 'none',
	'resource': 'katalog-admin-be',
	'realmPublicKey': 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl/1l8+xbTsVTh3k9GFRRWIsm1GS/BtIMFRetVLHmofz3MSNL18eS6nFsCnX7BH9YbQQHfi/v+52AoGUQjpaWF2+XrmexxcVDpXWqSvJCv8WQwaHVj6kmw1xIH/DieUid2svud/8KuVE9MgBYgwjZ+Gg+qV1MDEv6z+bdrG8cvCZJYduw53JFnIaLuAsLEi1H3XZ7UXP/aZ34E3r15cU2KbiLLyvN4ZiWyDDHbhDIsd69H3gB99AucDVgpeJlTEQYd/3QNhh9oGVwAZ1C9c71omwlFAuX/QmBchkM8w00t9R7H6siTEiTusl23eKUclEThRA4/3+RwhVpuiqQrjzQYwIDAQAB'
};

function initKeycloak() {

	console.log('initKeycloak');

	if (_keycloak) {
		console.log('Returning existing Keycloak instance!');
		return _keycloak;
	}
	else {
		console.log('Initializing Keycloak...');
		var memoryStore = new session.MemoryStore();
		_keycloak = new Keycloak({
			store: memoryStore,
			secret: 'eb7f33ac-fa92-4b00-ba96-61960138e137',
			resave: false,
			saveUninitialized: true
		}, keycloakConfig);
		return _keycloak;
	}
}

module.exports = {
	initKeycloak
};